/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package library;

import kea.zmirc.systemintegration.p1.shared.util.User;

/**
 *
 * @author razvanz
 */
public class Pump {

//    private static int pumpCounter = 0;
    public static int counter = 0;
    private static boolean isLocked = true;
    private static double pricePerLiter = 0.0;
    private static double pumpedGas = 0.0;
    private static double total = 0.0;
    private static String pumpName = "";
    private static int pumpNo;
    private static User user;

    public Pump(User user) {
        this.pumpName = "Gas pump no. " + String.valueOf(++Pump.counter);
        this.pumpNo = Pump.counter;
        this.user = user;
    }

    public static boolean isIsLocked() {
        return isLocked;
    }

    public static void setIsLocked(boolean isLocked) {
        Pump.isLocked = isLocked;
    }

    public static double getPricePerLiter() {
        return pricePerLiter;
    }

    public static void setPricePerLiter(double pricePerLiter) {
        Pump.pricePerLiter = pricePerLiter;
    }

    public static double getPumpedGas() {
        return pumpedGas;
    }

    public static void setPumpedGas(double pumpedGas) {
        Pump.pumpedGas = pumpedGas;
    }

    public static double getTotal() {
        return total;
    }

    public static void setTotal(double total) {
        Pump.total = total;
    }

    public static String getPumpName() {
        return pumpName;
    }

    public static void setPumpName(String pumpName) {
        Pump.pumpName = pumpName;
    }

    public static int getPumpNo() {
        return pumpNo;
    }

    public static void setPumpNo(int pumpNo) {
        Pump.pumpNo = pumpNo;
    }

    public static User getUser() {
        return user;
    }

    public static void setUser(User user) {
        Pump.user = user;
    }

}
