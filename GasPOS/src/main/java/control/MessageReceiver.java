/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package control;

import GUI.PumpsControllCenter;
import java.net.InetAddress;
import java.net.UnknownHostException;
import kea.zmirc.systemintegration.p1.shared.UDPSocket;
import kea.zmirc.systemintegration.p1.shared.util.User;
import library.Message;
import library.Pump;

/**
 *
 * @author Benjamucho
 *
 * This class creates a frame that sets the boundaries of the message
 */
public class MessageReceiver {

    // indicates beginning and end of packet
    private static final byte END = (byte) 0xC0;
    private static final byte ESC = (byte) 0xDB;

    private static final byte initPump = (byte) 0x06;
    private static final byte pumpStatus = (byte) 0x15;
    private static final byte getStatus = (byte) 0x15;
    private static final byte setLock = (byte) 0x25;
    private static final byte setPrice = (byte) 0x05;
    private static final byte resetPump = (byte) 0xf5;

    private static final String PROTOCOL_VER = "1.0.0";

    private static UDPSocket socket = new UDPSocket(User.Colautti_Matias_Benjamin, (dr, udps) -> {
        handleMessage(dr.getData(), dr.getSource());
    }, UDPSocket.DEFAULT_PORT, UDPSocket.DEFAULT_BUFFER_SIZE);

    private static Data posData;
    private static PumpsControllCenter ui = new PumpsControllCenter();

    public MessageReceiver() {
        this.ui.setVisible(true);
    }

    public static void handleMessage(byte[] message, User sender) {
        Message msg = null;
        try {
            msg = unframeMsg(message);
            if (msg != null) {

                switch (msg.getCommand()) {
                    case initPump:
                        posData.pumps.add(new Pump(sender));
                        ui.updatePumps(Data.pumps);
                        break;
                    case pumpStatus:
                        byte[] data = msg.getData();
                        
                        Pump activePump = ui.getActivePump();
                        if (activePump.getUser() == sender) {
//                            activePump.setIsLocked(isLocked);
//                            activePump.setPumpedGas(Double.valueOf(liters));
//                            activePump.setPricePerLiter(Double.valueOf(price));
                            activePump.setIsLocked(true);
                            activePump.setPumpedGas(Double.valueOf("852.25"));
                            activePump.setPricePerLiter(Double.valueOf("8.256"));
                            ui.updateActivePump(activePump);
                        }
                        System.out.print(data);
//                            for(int i=0,j=posData.pumps.size();i<j;i++){
//                                if(posData.pumps.get(i).getUser().equals(sender) && posData.pumps.get(i).getPumpNo() == pumpNo){
//                                    posData.pumps.get(i).setIsLocked(isLocked);
//                                    posData.pumps.get(i).setPumpedGas(Double.valueOf(liters));
//                                    posData.pumps.get(i).setPricePerLiter(Double.valueOf(price));
//                                }
//                            }
                        break;

                    default:
                        throw new Exception("Wrong command sent to the POS: \"" + msg.getCommand() + "\"");
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static Message unframeMsg(byte[] msg) throws Exception {
        System.out.println("Message Length " + msg.length);
        System.out.println("Mess" + msg.toString());
        for (int j = 0; j < msg.length; j++) {
            System.out.print(Integer.toHexString(msg[j]));
        }
        System.out.println("");

        byte command = msg[1];
        byte[] data = new byte[20];
        for (int i = 2; i < msg.length - 2 && msg[i] != END; i++) {
            data[i - 2] = msg[i];
        }
        return new Message(command, data);
    }

    public static void sendMsg(User user, byte command, byte[] data) throws UnknownHostException {
        socket.send(user, InetAddress.getByName("127.0.0.1"), 10001, prepareMsg(command, data));
    }

    private static byte[] prepareMsg(byte command, byte[] data) {
        byte[] msg = new byte[4 + data.length];
        msg[0] = END;
        msg[1] = command;
        int i = 0;
        for (i = 0; i < data.length; i++) {
            msg[i + 2] = data[i];
        }
        System.err.print(i);
        System.err.print(data.length);
        msg[i + 2] = 0x00;
        msg[i + 3] = END;

        System.err.print("command  :  ");
        System.err.print(command);
        System.err.println();
        System.err.println(msg.length);

        for (int j = 0; j < msg.length; j++) {
            System.out.print(Integer.toHexString(msg[j]));
        }
        System.out.println("");
        return msg;
    }

    public static void getStatus(Pump pump) {
        try {
            sendMsg(pump.getUser(), getStatus, new byte[0]);
        } catch (UnknownHostException e) {
            e.printStackTrace();
        }

    }

    public static void resetCounter(Pump pump) {
        try {
            sendMsg(pump.getUser(), resetPump, new byte[0]);
        } catch (UnknownHostException e) {
            e.printStackTrace();
        }

    }

    public static void setPrice(Pump pump, double price) {
        try {
            sendMsg(pump.getUser(), setPrice, convertDoubleToByteArray(price));
        } catch (UnknownHostException e) {
            e.printStackTrace();
        }

    }

    public static void setLock(Pump pump, boolean lock) {
        byte[] lockData = new byte[1];
        lockData[0] = lock == true ? (byte) 0x01 : (byte) 0x00;
        try {
            sendMsg(pump.getUser(), setLock, lockData);
        } catch (UnknownHostException e) {
            e.printStackTrace();
        }

    }

    private static byte[] convertDoubleToByteArray(double d) {
        byte[] output = new byte[8];
        long lng = Double.doubleToLongBits(d);
        for (int i = 0; i < 8; i++) {
            output[i] = (byte) ((lng >> ((7 - i) * 8)) & 0xff);
        }
        return output;
    }
}
