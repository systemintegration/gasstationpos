/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package GUI;

import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Pattern;
import javax.swing.JLabel;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import library.Pump;

/**
 *
 * @author razvanz
 */
public class PumpControllCenter extends javax.swing.JFrame {

    private Pump pump = null;
    private static final Pattern DOUBLE_PATTERN = Pattern.compile(
            "[\\x00-\\x20]*[+-]?(NaN|Infinity|((((\\p{Digit}+)(\\.)?((\\p{Digit}+)?)"
            + "([eE][+-]?(\\p{Digit}+))?)|(\\.((\\p{Digit}+))([eE][+-]?(\\p{Digit}+))?)|"
            + "(((0[xX](\\p{XDigit}+)(\\.)?)|(0[xX](\\p{XDigit}+)?(\\.)(\\p{XDigit}+)))"
            + "[pP][+-]?(\\p{Digit}+)))[fFdD]?))[\\x00-\\x20]*");
    
    /**
     * Creates new form PumpControllCenter
     * @param pump
     */
    public PumpControllCenter() {
        initComponents();
    }

    public void update(Pump pump){
        this.pump = pump;
        
        PumpNameLbl.setText(pump.getPumpName());
        String status = pump.isIsLocked() == true ? "LOCKED" : "UNLOCKED";
        isLockedLbl.setText(status);
        priceValLbl.setText(String.valueOf(pump.getPricePerLiter()));
        litersValInput.setText(String.valueOf(pump.getPumpedGas()));
        totalValLbl.setText(String.valueOf(pump.getPumpedGas() * pump.getPricePerLiter()));
    }
    
    public void setMessage(String data){
        msgValTextarea.setText(data);
    }
    
    public boolean isDouble(String str) {
        return DOUBLE_PATTERN.matcher(str).matches();
    }
    
    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        PumpNameLbl = new javax.swing.JLabel();
        totalLbl = new javax.swing.JLabel();
        totalValLbl = new javax.swing.JLabel();
        statusLbl = new javax.swing.JLabel();
        isLockedLbl = new javax.swing.JLabel();
        LitersLbl = new javax.swing.JLabel();
        litersValInput = new javax.swing.JTextField();
        PriceLbl = new javax.swing.JLabel();
        priceValLbl = new javax.swing.JLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        msgValTextarea = new javax.swing.JTextArea();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        PumpNameLbl.setText("Gas Pump 1");
        PumpNameLbl.setToolTipText("");

        totalLbl.setText("Total:");

        totalValLbl.setText("9999.99");
        totalValLbl.setToolTipText("");

        statusLbl.setText("Status:");

        isLockedLbl.setText("LOCKED");

        LitersLbl.setText("Liters:");

        litersValInput.setText("132.44");
        litersValInput.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                litersValInputKeyReleased(evt);
            }
        });

        PriceLbl.setText("Price per liter:");

        priceValLbl.setText("120.22");

        msgValTextarea.setColumns(20);
        msgValTextarea.setRows(5);
        jScrollPane1.setViewportView(msgValTextarea);

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(PumpNameLbl, javax.swing.GroupLayout.DEFAULT_SIZE, 376, Short.MAX_VALUE)
                .addContainerGap())
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(25, 25, 25)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 318, Short.MAX_VALUE)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(LitersLbl, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(statusLbl, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(totalLbl, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(PriceLbl, javax.swing.GroupLayout.DEFAULT_SIZE, 150, Short.MAX_VALUE))
                        .addGap(18, 18, 18)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(totalValLbl, javax.swing.GroupLayout.DEFAULT_SIZE, 150, Short.MAX_VALUE)
                            .addComponent(isLockedLbl, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(litersValInput)
                            .addComponent(priceValLbl, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(PumpNameLbl)
                .addGap(50, 50, 50)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(statusLbl)
                    .addComponent(isLockedLbl))
                .addGap(18, 18, 18)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(LitersLbl)
                    .addComponent(litersValInput, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(PriceLbl)
                    .addComponent(priceValLbl))
                .addGap(18, 18, 18)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(totalLbl)
                    .addComponent(totalValLbl))
                .addGap(18, 18, 18)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(19, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void litersValInputKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_litersValInputKeyReleased
        String liters = litersValInput.getText();
//        if (!((evt.getKeyCode() >= 96 && evt.getKeyCode() <= 105 || 
//                    evt.getKeyCode() >= 48 && evt.getKeyCode() <= 57 || 
//                    evt.getKeyCode() >= 37 && evt.getKeyCode() <= 40) || 
//                    evt.getKeyCode() == 46 || evt.getKeyCode() == 8 || evt.getKeyCode() == 190)){
//            if (liters.length() > 0){
//                if(!isDouble(liters.substring(liters.length() - 1))) {
//                    litersValInput.setText(liters.substring(0,liters.length() - 1));
//                     return;           
//                }
//            }
//        }
        try{
            this.pump.setPumpedGas(Double.valueOf(liters));
            this.pump.setTotal(this.pump.getPumpedGas() * this.pump.getPricePerLiter());
            totalValLbl.setText(String.valueOf(this.pump.getTotal()));
            return;
        }catch(Exception e){
            return;
        }
//        
    }//GEN-LAST:event_litersValInputKeyReleased


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel LitersLbl;
    private javax.swing.JLabel PriceLbl;
    private javax.swing.JLabel PumpNameLbl;
    private javax.swing.JLabel isLockedLbl;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTextField litersValInput;
    private javax.swing.JTextArea msgValTextarea;
    private javax.swing.JLabel priceValLbl;
    private javax.swing.JLabel statusLbl;
    private javax.swing.JLabel totalLbl;
    private javax.swing.JLabel totalValLbl;
    // End of variables declaration//GEN-END:variables

    public JLabel getPumpNameLbl() {
        return PumpNameLbl;
    }

    public void setPumpNameLbl(JLabel PumpNameLbl) {
        this.PumpNameLbl = PumpNameLbl;
    }

    public JLabel getIsLockedLbl() {
        return isLockedLbl;
    }

    public void setIsLockedLbl(JLabel isLockedLbl) {
        this.isLockedLbl = isLockedLbl;
    }

    public JTextField getLitersValInput() {
        return litersValInput;
    }

    public void setLitersValInput(JTextField litersValInput) {
        this.litersValInput = litersValInput;
    }

    public JTextArea getMsgValTextarea() {
        return msgValTextarea;
    }

    public void setMsgValTextarea(JTextArea msgValTextarea) {
        this.msgValTextarea = msgValTextarea;
    }

    public JLabel getPriceValLbl() {
        return priceValLbl;
    }

    public void setPriceValLbl(JLabel priceValLbl) {
        this.priceValLbl = priceValLbl;
    }

    public JLabel getTotalValLbl() {
        return totalValLbl;
    }

    public void setTotalValLbl(JLabel totalValLbl) {
        this.totalValLbl = totalValLbl;
    }

    
}

