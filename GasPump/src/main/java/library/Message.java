/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package library;

/**
 *
 * @author razvanz
 */
public class Message {
    
    public Message(byte command, byte[] data){
        this.command = command;
        this.data = data;        
    }
    
    private byte command = (byte) 0x00;
    private byte[] data;

    public byte getCommand() {
        return command;
    }

    public void setCommand(byte command) {
        this.command = command;
    }

    public byte[] getData() {
        return data;
    }

    public void setData(byte[] data) {
        this.data = data;
    }

    
}
