/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package library;

import kea.zmirc.systemintegration.p1.shared.util.User;

/**
 *
 * @author razvanz
 */
public class Pump {
    
//    private static int pumpCounter = 0;

    public static int counter = 0;
    private  boolean isLocked = true;
    private  double pricePerLiter = 0.0;
    private  double pumpedGas = 0.0;
    private  double total = 0.0;
    private  String pumpName = "";
    private  int pumpNo;
    private  User user;

    public Pump( User user){
        this.pumpName = "Gas pump no. " +String.valueOf(++counter);
        this.pumpNo = counter;
        this.user = user;
    }

    public  boolean isIsLocked() {
        return isLocked;
    }

    public  void setIsLocked(boolean isLocked) {
        this.isLocked = isLocked;
    }

    public  double getPricePerLiter() {
        return pricePerLiter;
    }

    public  void setPricePerLiter(double pricePerLiter) {
        this.pricePerLiter = pricePerLiter;
    }

    public  double getPumpedGas() {
        return pumpedGas;
    }

    public  void setPumpedGas(double pumpedGas) {
        this.pumpedGas = pumpedGas;
    }

    public  double getTotal() {
        return total;
    }

    public  void setTotal(double total) {
        this.total = total;
    }

    public  String getPumpName() {
        return pumpName;
    }

    public  void setPumpName(String pumpName) {
        this.pumpName = pumpName;
    }

    public  int getPumpNo() {
        return pumpNo;
    }

    public  void setPumpNo(int pumpNo) {
        this.pumpNo = pumpNo;
    }
    
    public User getUser() {
        return user;
    }

    public  void setUser(User user) {
        this.user = user;
    }
    
}
