/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package control;

import GUI.PumpControllCenter;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.nio.ByteBuffer;
import kea.zmirc.systemintegration.p1.shared.UDPSocket;
import kea.zmirc.systemintegration.p1.shared.util.User;
import library.Message;
import library.Pump;

/**
 *
 * @author Benjamucho
 *
 * This class creates a frame that sets the boundaries of the message
 */
public class MessageReceiver {

    // indicates beginning and end of packet
    private static final byte END = (byte) 0xC0;
    private static final byte ESC = (byte) 0xDB;

    private static final byte initPump = (byte) 0x06;
    private static final byte pumpStatus = (byte) 0x15;
    private static final byte getStatus = (byte) 0x15;
    private static final byte setLock = (byte) 0x25;
    private static final byte setPrice = (byte) 0x05;
    private static final byte resetPump = (byte) 0xf5;

    private static final String PROTOCOL_VER = "1.0.0";

    private static UDPSocket socket = new UDPSocket(User.Colautti_Matias_Benjamin, (dr, udps) -> {
        handleMessage(dr.getData(), dr.getSource());
    }, 10001, UDPSocket.DEFAULT_BUFFER_SIZE);

    private static Pump pump = new Pump(User.Colautti_Matias_Benjamin);
    private static PumpControllCenter ui = new PumpControllCenter();

    public MessageReceiver() {
        this.ui.setVisible(true);
        initPump(pump);
        ui.update(pump);
    }

    public static void handleMessage(byte[] message, User sender) {
        Message msg = null;
        try {
            msg = unframeMsg(message);
            if (msg != null) {

                switch (msg.getCommand()) {
                    case getStatus:
                        System.out.print(msg.getData());
                        ui.setMessage(String.valueOf(msg.getData()));
                        pumpStatus(pump);
                        break;
                    case setPrice:
                        System.out.print(msg.getData());
                        ui.setMessage(String.valueOf(msg.getData()));
                        pump.setPricePerLiter(convertByteArraytoDouble(msg.getData()));
                        ui.update(pump);
                        break;
                    case setLock:
                        System.out.print(msg.getData());
                        ui.setMessage(String.valueOf(msg.getData()));
                        boolean lock = convertByteArraytoDouble(msg.getData()) != 0 ? false : true;
                        pump.setIsLocked(lock);
                        ui.update(pump);
                        break;
                    case resetPump:
                        System.out.print(msg.getData());
                        ui.setMessage(String.valueOf(msg.getData()));
                        pump.setPumpedGas(0);
                        ui.update(pump);
                        break;

                    default:
                        throw new Exception("Wrong command sent to the POS: \"" + msg.getCommand() + "\"");
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static Message unframeMsg(byte[] msg) throws Exception {
        System.out.println("Message Length " + msg.length);
        System.out.println("Mess" + msg.toString());

        for (int j = 0; j < msg.length; j++) {
            System.out.print(Integer.toHexString(msg[j]));
        }
        System.out.println("");

        byte command = msg[1];
        byte[] data = new byte[20];
        for (int i = 2; i < msg.length - 2 && msg[i] != END; i++) {
            data[i - 2] = msg[i];
        }
        return new Message(command, data);
    }

    public static void sendMsg(User user, byte command, byte[] data) throws UnknownHostException {
        socket.send(user, InetAddress.getByName("127.0.0.1"), UDPSocket.DEFAULT_PORT, prepareMsg(command, data));
    }

    private static byte[] prepareMsg(byte command, byte[] data) {
        byte[] msg = new byte[30];
        msg[0] = END;
        msg[1] = command;
        int i = 0;
        for (i = 0; i < data.length; i++) {
            msg[i + 2] = data[i];
        }
        msg[i + 3] = 0;
        msg[i + 4] = END;

        System.out.print(msg);

        return msg;
    }

    public static void initPump(Pump pump) {
        try {
            sendMsg(pump.getUser(), getStatus, new byte[0]);
        } catch (UnknownHostException e) {
            e.printStackTrace();
        }

    }

    public static void pumpStatus(Pump pump) {
        byte[] pumpStatusData = new byte[17], liters, price;
        pumpStatusData[0] = pump.isIsLocked() == true ? (byte) 0x01 : (byte) 0x00;
        liters = convertDoubleToByteArray(pump.getPumpedGas());
        price = convertDoubleToByteArray(pump.getPricePerLiter());
        for (int i = 0; i < 8; i++) {
            pumpStatusData[i + 1] = liters[i];
            pumpStatusData[i + 9] = price[i];
        }

        try {
            sendMsg(pump.getUser(), pumpStatus, pumpStatusData);
        } catch (UnknownHostException e) {
            e.printStackTrace();
        }

    }

    private static byte[] convertDoubleToByteArray(double d) {
        byte[] output = new byte[8];
        long lng = Double.doubleToLongBits(d);
        for (int i = 0; i < 8; i++) {
            output[i] = (byte) ((lng >> ((7 - i) * 8)) & 0xff);
        }
        return output;
    }

    public static double convertByteArraytoDouble(byte[] bytes) {
        return ByteBuffer.wrap(bytes).getDouble();
    }
}
